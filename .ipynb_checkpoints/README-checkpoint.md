# GEOROC 2.0 Jupyter-Notebooks

Developing Jupyter Notebooks to demonstrate capabilities of GEOROC 2.0. Specifically data access via the API followed by suggested workflows for data formatting, cleaning, re-sampling and plotting.

In the first instance, used to test API. 
