<img src="https://pbs.twimg.com/media/E0m6c9FX0AIX0p1.png" style="height:50px" align="left"/> <br><br>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>


***

The GEOROC Database (Geochemistry of Rocks of the Oceans and Continents) is a comprehensive collection of published analyses <br/> of igneous and metamorphic rocks and minerals. It contains major and trace element concentrations, radiogenic and nonradiogenic <br/> isotope ratios as well as analytical ages for whole rocks, glasses, minerals and inclusions. Metadata include geospatial and other <br/> sample information, analytical details and references.

The GEOROC Database was established at the Max Planck Institute for Chemistry in Mainz (Germany). In 2021, the database was <br/> moved to Göttingen University, where it continues to be curated as part of the DIGIS project of the Department of Geochemistry and <br/> Isotope Geology at the Geoscience Centre (GZG) and the University and State Library (SUB). Development for GEOROC 2.0 <br/> includes a new data model for greater interoperability, options to contribute data, and improved access to the database.

As part of the DIGIS project, a new API interface has been created for the GEOROC database, allowing easy access to its contents <br/>
with simple programming skills. Users can now query the database and retrieve data using the new API, making it more accessible <br/>
and useful for researchers and other interested parties. This notebook demonstrates the basic capabilities of GEOROC data access <br/> via the new DIGIS API. 

For feedback, questions and further information contact [Digis-Info](mailto:digis-info@uni-goettingen.de) directly.

***

# Jupyter-Notebooks

The Jupyter Notebooks created are specifically designed to illustrate the features of GEOROC 2.0. They focus on data access via <br/> the API, followed by suggested workflows for data formatting, cleansing, resampling, and presentation. They were first used to test <br/> the API and ensure its functionality.

The Jupyter Notebooks are divided into three specific categories:

1. The first Jupyter Notebook is focused on demonstrating how to send API requests. It illustrates how users can query the API and <br/> retrieve data from the GEOROC database. The examples in this notebook cover a wide range of API requests and demonstrate <br/> the usability and accessibility of the new API.

2. The second notebook focuses on general methods for data cleansing. It illustrates how data retrieved from the API can be cleaned, <br/> preprocessed, and formatted for further analysis. The methods presented in this notebook are universal and can be applied to all <br/> types of data retrieved from the GEOROC database.

3. The third notebook focuses on data visualization and includes a detailed reconstruction of a geochemical publication. This notebook <br/> illustrates how the cleaned and formatted data can be visualized to provide valuable insight and interpretation. It illustrates various <br/> methods of data visualization and how they can be used to present results in a clear and understandable manner.

Each of these notebooks includes detailed tutorials and explanations to help users understand the functionality of the API and the processes <br/> of data cleaning and visualization.

***

### Usage:

Launch our Notebooks: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/georoc%2FJupyter/HEAD?labpath=DIGIS_GeoRoc.ipynb)

***

### Social:

[Visit our Website](https://georoc.mpch-mainz.gwdg.de/georoc/)


![Twitter Follow](https://img.shields.io/twitter/follow/DIGISgeo?style=social)

***

### Authors:

* **Dr. Marthe Klöcking** - *DIGIS Project Co-ordinator* - [Dr. Marthe Klöcking](https://www.uni-goettingen.de/en/644510.html)
* **Timm M. Wirtz** - *Student in geoscience* - [Timm Wirtz](https://github.com/tmwProjects/)